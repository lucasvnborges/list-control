## Configuração do projeto

- Faça o clone do projeto em sua máquina
- Abra o projeto e em seguida a pasta `android`
- Adicione na raiz da pasta android um arquivo com nome `local.properties`
- Dentro do arquivo `local.properties`, adicione o seguinte código `sdk.dir={o caminho configurado na variável ANDROID_HOME}` ex.: `sdk.dir=/home/lucas/Android/Sdk`

Faça a instalação dos pacotes e dependências

```bash
yarn install
```

Com um emulador ou dispositivo android conectado via usb, basta rodar o seguinte comando

```
yarn android
```

