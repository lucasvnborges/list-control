import React, { useCallback, useEffect, useMemo } from "react";
import { useUser } from "@realm/react";
import { Appbar } from "react-native-paper";
import { List } from "./models/List";
import { RealmContext } from "./models";
import { Home } from "./components/Home";

const { useRealm, useQuery } = RealmContext;

export const AppSync: React.FC = () => {
  const realm = useRealm();
  const user = useUser();
  const queryLists = useQuery(List);

  const lists = useMemo(() => queryLists.sorted("createdAt"), [queryLists]);

  useEffect(() => {
    realm.subscriptions.update((mutableSubs) => {
      mutableSubs.add(realm.objects(List));
    });
  }, [realm, queryLists]);

  const handleLogout = useCallback(() => {
    user?.logOut();
  }, [user]);

  return (
    <>
      <Appbar.Header>
        <Appbar.Content title="Minhas listas" />
        <Appbar.Action
          icon="account-arrow-right-outline"
          onPress={handleLogout}
        />
      </Appbar.Header>
      <Home lists={lists} userId={user?.id} />
    </>
  );
};
