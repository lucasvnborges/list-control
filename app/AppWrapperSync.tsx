import React from "react";
import { AppProvider, UserProvider } from "@realm/react";
import {
  MD3LightTheme as DefaultTheme,
  Provider as PaperProvider,
} from "react-native-paper";
import {
  SafeAreaProvider,
  initialWindowMetrics,
} from "react-native-safe-area-context";
import Navigation from "./navigation";
import { ToastProvider } from "react-native-paper-toast";
import { RealmContext } from "./models";
import { LoginScreen } from "./components/LoginScreen";
import { StatusBar } from "expo-status-bar";

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: "#fff",
    primary: "#34454e",
    primaryContainer: "#fdc839",
    onPrimaryContainer: "#34454e",
  },
};

export const AppWrapperSync: React.FC<{
  appId: string;
}> = ({ appId }) => {
  const { RealmProvider } = RealmContext;

  return (
    <SafeAreaProvider initialMetrics={initialWindowMetrics}>
      <PaperProvider theme={theme}>
        <ToastProvider>
          <AppProvider id={appId}>
            <UserProvider fallback={LoginScreen}>
              <RealmProvider sync={{ flexible: true }}>
                <Navigation theme={theme} />
                <StatusBar style="dark" />
              </RealmProvider>
            </UserProvider>
          </AppProvider>
        </ToastProvider>
      </PaperProvider>
    </SafeAreaProvider>
  );
};

export default AppWrapperSync;
