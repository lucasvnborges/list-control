import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import { IconButton, TextInput, useTheme } from "react-native-paper";

type AddTaskFormProps = {
  onSubmit: (description: string) => void;
};

export const AddTaskForm: React.FC<AddTaskFormProps> = ({ onSubmit }) => {
  const theme = useTheme();

  const [description, setDescription] = useState("");

  const handleSubmit = () => {
    onSubmit(description);
    setDescription("");
  };

  return (
    <View style={styles.form}>
      <TextInput
        mode="outlined"
        value={description}
        blurOnSubmit={false}
        style={styles.textInput}
        label="Descrição da tarefa"
        onChangeText={setDescription}
        onSubmitEditing={handleSubmit}
      />

      <IconButton
        size={20}
        icon="plus"
        mode="contained"
        onPress={handleSubmit}
        style={styles.iconButtonStyle}
        iconColor={theme.colors.background}
        containerColor={theme.colors.primary}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  form: {
    marginVertical: 12,
    flexDirection: "row",
    paddingHorizontal: 12,
    alignItems: "flex-end",
    justifyContent: "space-between",
  },
  textInput: {
    flex: 1,
    marginRight: 8,
  },
  iconButtonStyle: {
    width: 54,
    margin: 0,
    height: "90%",
    borderRadius: 4,
  },
});
