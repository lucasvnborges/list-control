import React from "react";
import { StyleSheet, View, Image } from "react-native";
import { Text } from "react-native-paper";

const uri =
  "https://res.cloudinary.com/drj6rtiuz/image/upload/v1670565145/reading-list-cuate-3652_1_y6r81x.png";

export const EmptyList = () => {
  return (
    <View style={styles.content}>
      <Image style={styles.image} source={{ uri }} />

      <View style={styles.textContainer}>
        <Text variant="titleMedium" style={styles.text}>
          Boas-vindas ao aplicativo ListControl
        </Text>
        <Text variant="bodyLarge" style={[styles.text, styles.paragraph]}>
          Toque no botão "+" para criar sua primeira lista
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: 12,
    justifyContent: "center",
  },
  image: {
    flex: 1,
  },
  textContainer: {
    flex: 1,
    marginTop: 32,
  },
  text: {
    textAlign: "center",
  },
  paragraph: {
    marginTop: 4,
  },
});
