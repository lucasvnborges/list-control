import React from "react";
import { View, StyleSheet } from "react-native";
import { List } from "../models/List";
import { ListItens } from "./ListItens";
import { EmptyList } from "./EmptyList";
import { FAB } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import { realmToPlainObject } from "../utils";

export const Home: React.FC<{
  lists: Realm.Results<List & Realm.Object>;
}> = ({ lists }) => {
  const navigation = useNavigation();

  const handleCreateList = () => {
    navigation.navigate("ListManager");
  };

  const handleUpdateList = (list: List) => {
    const serializedList = realmToPlainObject(list);

    navigation.navigate("ListManager", { list: serializedList });
  };

  const handleOpenList = (list: List) => {
    const serializedList = realmToPlainObject(list);

    navigation.navigate("TaskManager", { list: serializedList });
  };

  return (
    <View style={styles.content}>
      {lists.length === 0 ? (
        <EmptyList />
      ) : (
        <ListItens
          lists={lists}
          openList={handleOpenList}
          upadteList={handleUpdateList}
        />
      )}

      <FAB
        icon="plus"
        mode="flat"
        style={styles.fab}
        label="Criar lista"
        onPress={handleCreateList}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 20,
  },
  fab: {
    position: "absolute",
    right: 0,
    bottom: 24,
    margin: 16,
  },
});
