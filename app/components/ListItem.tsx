import React, { useMemo } from "react";
import Realm from "realm";
import moment from "moment";
import { View, StyleSheet } from "react-native";
import { Text, IconButton, TouchableRipple } from "react-native-paper";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { List } from "../models/List";

type ListItemProps = {
  list: List & Realm.Object;
  openList: (list: List) => void;
  upadteList: (list: List) => void;
};

export const ListItem = React.memo<ListItemProps>(
  ({ list, openList, upadteList }) => {
    const createdAt = useMemo(
      () => moment(list.createdAt).format("DD-MM-YYYY"),
      [list.createdAt]
    );

    return (
      <View style={styles.list}>
        <TouchableRipple
          borderless
          style={styles.touchable}
          onPress={() => openList(list)}
        >
          <View style={styles.itemWrapper}>
            <View style={styles.row}>
              <View style={styles.column}>
                <Text variant="labelLarge">
                  <MaterialCommunityIcons
                    size={16}
                    color="#333"
                    name="nature-people"
                  />{" "}
                  Fazendeiro
                </Text>
                <Text>{list.nomeFazendeiro}</Text>
              </View>
              <IconButton
                size={20}
                iconColor="#555"
                icon="playlist-edit"
                style={{ margin: 0 }}
                onPress={() => upadteList(list)}
              />
            </View>
            <View style={styles.column}>
              <Text variant="labelLarge">
                <MaterialCommunityIcons
                  name="warehouse"
                  size={14}
                  color="#333"
                />{" "}
                Fazenda
              </Text>
              <Text>{list.nomeFazenda}</Text>
            </View>

            <View style={styles.row}>
              <View style={styles.column}>
                <Text variant="labelLarge">
                  <MaterialCommunityIcons
                    name="city-variant-outline"
                    size={14}
                    color="#333"
                  />{" "}
                  Cidade
                </Text>
                <Text>{list.cidade}</Text>
              </View>
              <View style={styles.column}>
                <Text variant="labelLarge">Lista criada em</Text>
                <Text>{createdAt}</Text>
              </View>
            </View>
          </View>
        </TouchableRipple>
      </View>
    );
  }
);

const styles = StyleSheet.create({
  list: {
    borderWidth: 1,
    borderRadius: 12,
    marginBottom: 12,
    borderColor: "#eee",
  },
  itemWrapper: {
    padding: 24,
  },
  touchable: {
    borderRadius: 11,
  },
  column: {
    marginBottom: 4,
    flexDirection: "column",
  },
  row: {
    marginTop: 4,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
