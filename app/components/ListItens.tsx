import React from "react";
import { View, FlatList, StyleSheet } from "react-native";
import { Realm } from "@realm/react";
import { List } from "../models/List";
import { ListItem } from "./ListItem";

type TaskListProps = {
  lists: Realm.Results<List & Realm.Object>;
  openList: (list: List & Realm.Object) => void;
  upadteList: (list: List & Realm.Object) => void;
};

export const ListItens: React.FC<TaskListProps> = ({
  lists,
  openList,
  upadteList,
}) => {
  return (
    <View style={styles.listContainer}>
      <FlatList
        data={lists}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flatList}
        keyExtractor={(list) => list._id.toString()}
        renderItem={({ item }) => (
          <ListItem list={item} openList={openList} upadteList={upadteList} />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  listContainer: {
    flex: 1,
    justifyContent: "center",
  },
  flatList: {
    paddingBottom: 144,
  },
});

export default ListItens;
