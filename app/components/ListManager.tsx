import * as Yup from "yup";
import moment from "moment";
import React, { useCallback, useRef } from "react";
import { StyleSheet, View, ScrollView } from "react-native";
import {
  Text,
  TextInput,
  Switch,
  RadioButton,
  Button,
  Appbar,
  Portal,
  Dialog,
  Paragraph,
} from "react-native-paper";
import { useFormik } from "formik";
import { Realm, useUser } from "@realm/react";
import { RealmContext } from "../models";
import { List } from "../models/List";
import { difference } from "../utils";
import { useNavigation } from "@react-navigation/native";
import { useToast } from "react-native-paper-toast";

const { useRealm } = RealmContext;

const ListFormSchema = Yup.object().shape({
  nomeFazendeiro: Yup.string().required("Required"),
  nomeFazenda: Yup.string().required("Required"),
  cidade: Yup.string().required("Required"),
  nomeSupervisor: Yup.string().required("Required"),
  tipoChecklist: Yup.string().required("Required"),
  leiteProduzido: Yup.string(),
  quantidadeGado: Yup.string(),
  supervisaoMensal: Yup.boolean(),
});

export const ListManager = (props) => {
  const { params } = props.route;

  const nomeFazendeiro = useRef(null);
  const nomeFazenda = useRef(null);
  const cidade = useRef(null);
  const nomeSupervisor = useRef(null);
  const leiteProduzido = useRef(null);
  const quantidadeGado = useRef(null);

  const realm = useRealm();
  const user = useUser();
  const navigation = useNavigation();
  const toaster = useToast();

  const [visible, setVisible] = React.useState(false);

  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const handleAddList = useCallback(
    (formValues: any): void => {
      realm.write(() => {
        return new List(realm, formValues, user.id);
      });

      navigation.goBack();
      toaster.show({
        type: "success",
        duration: 2500,
        position: "top",
        message: "uma nova lista foi adicionada!",
      });
    },
    [realm, user]
  );

  const handleUpdateList = useCallback(
    (list: List & Realm.Object, formValues: any): void => {
      const oldObject = list;
      const newObject = { ...list, ...formValues };
      const diff: any = difference(oldObject, newObject);

      if (Object.keys(diff).length === 0) return;

      if (diff.quantidadeGado) {
        diff.rebanhoAtualizadoEm = new Date();
      }

      realm.write(() => {
        const listToUpdate = realm?.objectForPrimaryKey(
          "List",
          new Realm.BSON.ObjectId(list._id)
        );

        Object.keys(diff).forEach((key) => {
          listToUpdate[key] = diff[key];
        });

        navigation.goBack();
        toaster.show({
          type: "info",
          duration: 2500,
          position: "top",
          message: "dados atualizados!",
        });
      });
    },
    [realm]
  );

  const handleDeleteList = useCallback((): void => {
    realm.write(() => {
      realm.delete(
        realm.objectForPrimaryKey(
          "List",
          new Realm.BSON.ObjectId(params.list._id)
        )
      );

      navigation.goBack();
      toaster.show({
        type: "warning",
        duration: 2500,
        position: "top",
        message: "lista excluída!",
      });
    });
  }, [realm]);

  const {
    handleChange,
    handleSubmit,
    handleBlur,
    values,
    setFieldValue,
    errors,
  } = useFormik({
    validationSchema: ListFormSchema,
    initialValues: {
      nomeFazendeiro: params?.list?.nomeFazendeiro || "",
      nomeFazenda: params?.list?.nomeFazenda || "",
      cidade: params?.list?.cidade || "",
      nomeSupervisor: params?.list?.nomeSupervisor || "",
      tipoChecklist: params?.list?.tipoChecklist || "BPA",
      leiteProduzido: params?.list?.leiteProduzido || "",
      quantidadeGado: params?.list?.quantidadeGado || "",
      supervisaoMensal: params?.list?.supervisaoMensal || false,
    },
    onSubmit: (values) => handleAddList(values),
  });

  const handleChangeSupervisao = (value: boolean) => {
    setFieldValue("supervisaoMensal", value);
  };

  return (
    <View style={styles.container}>
      <Appbar.Header>
        <Appbar.Content
          title={params?.list._id ? "Atualizar lista" : "Criar nova lista"}
          titleStyle={{ fontSize: 16, fontWeight: "700", marginLeft: 6 }}
        />
        <Appbar.Action icon="close" onPress={() => props.navigation.goBack()} />
      </Appbar.Header>

      <ScrollView
        keyboardShouldPersistTaps="always"
        contentContainerStyle={styles.content}
      >
        <TextInput
          mode="outlined"
          returnKeyType="next"
          ref={nomeFazendeiro}
          style={styles.input}
          blurOnSubmit={false}
          label="Nome do fazendeiro"
          autoFocus={!params?.list._id}
          value={values.nomeFazendeiro}
          onBlur={handleBlur("nomeFazendeiro")}
          error={Boolean(errors.nomeFazendeiro)}
          onChangeText={handleChange("nomeFazendeiro")}
          onSubmitEditing={() => nomeFazenda.current?.focus()}
        />
        <TextInput
          mode="outlined"
          ref={nomeFazenda}
          returnKeyType="next"
          style={styles.input}
          blurOnSubmit={false}
          label="Nome da fazenda"
          value={values.nomeFazenda}
          onBlur={handleBlur("nomeFazenda")}
          error={Boolean(errors.nomeFazenda)}
          onChangeText={handleChange("nomeFazenda")}
          onSubmitEditing={() => cidade.current?.focus()}
        />
        <TextInput
          ref={cidade}
          mode="outlined"
          label="Cidade"
          returnKeyType="next"
          style={styles.input}
          blurOnSubmit={false}
          value={values.cidade}
          onBlur={handleBlur("cidade")}
          error={Boolean(errors.cidade)}
          onChangeText={handleChange("cidade")}
          onSubmitEditing={() => nomeSupervisor.current?.focus()}
        />
        <TextInput
          mode="outlined"
          style={styles.input}
          ref={nomeSupervisor}
          label="Nome do supervisor"
          value={values.nomeSupervisor}
          onBlur={handleBlur("nomeSupervisor")}
          error={Boolean(errors.nomeSupervisor)}
          onChangeText={handleChange("nomeSupervisor")}
        />

        <RadioButton.Group
          value={values.tipoChecklist}
          onValueChange={(newValue) => setFieldValue("tipoChecklist", newValue)}
        >
          <View style={{ marginVertical: 12 }}>
            <Text variant="titleSmall" style={styles.checklistTypeTitle}>
              Tipo de checklist
            </Text>
            <View style={styles.checklistTypeWrapper}>
              <RadioButton value="BPA" />
              <Text variant="titleSmall" style={styles.checklistTypeText}>
                BPA
              </Text>
            </View>
            <View style={styles.checklistTypeWrapper}>
              <RadioButton value="Antibiótico" />
              <Text variant="titleSmall" style={styles.checklistTypeText}>
                Antibiótico
              </Text>
            </View>
            <View style={styles.checklistTypeWrapper}>
              <RadioButton value="BPF" />
              <Text variant="titleSmall" style={styles.checklistTypeText}>
                BPF
              </Text>
            </View>
          </View>
        </RadioButton.Group>

        <TextInput
          mode="outlined"
          returnKeyType="next"
          ref={leiteProduzido}
          style={styles.input}
          blurOnSubmit={false}
          keyboardType="number-pad"
          value={values.leiteProduzido}
          error={Boolean(errors.leiteProduzido)}
          label="Qtd de leite produzido no mês"
          onBlur={handleBlur("leiteProduzido")}
          right={<TextInput.Affix text="Litros" />}
          onChangeText={handleChange("leiteProduzido")}
          onSubmitEditing={() => quantidadeGado.current?.focus()}
        />
        <TextInput
          mode="outlined"
          ref={quantidadeGado}
          style={styles.input}
          returnKeyType="done"
          keyboardType="number-pad"
          value={values.quantidadeGado}
          label="Qtd de cabeças de gado"
          onBlur={handleBlur("quantidadeGado")}
          error={Boolean(errors.quantidadeGado)}
          onChangeText={handleChange("quantidadeGado")}
        />
        {params?.list._id && params.list.quantidadeGado !== "" && (
          <Text variant="bodySmall">
            Rebanho atualizado em:{" "}
            {moment(params.list.rebanhoAtualizadoEm).format(
              "DD/MM/YYYY 🕐 hh:mm"
            )}
          </Text>
        )}

        <View style={styles.supervisionContainer}>
          <View style={styles.column}>
            <Text variant="titleSmall">Teve supervisão no mês em curso?</Text>
            <Text variant="bodyMedium">
              {values.supervisaoMensal ? "sim" : "não"}
            </Text>
          </View>
          <Switch
            value={values.supervisaoMensal}
            onValueChange={handleChangeSupervisao}
          />
        </View>

        <Button
          mode="contained"
          style={styles.buttonStyle}
          labelStyle={styles.labelStyle}
          contentStyle={styles.buttonContentStyle}
          onPress={() =>
            params?.list._id
              ? handleUpdateList(params?.list, values)
              : handleSubmit()
          }
        >
          {params?.list._id ? "Salvar lista" : "Criar lista"}
        </Button>
        {params?.list._id && (
          <Button
            mode="outlined"
            onPress={showDialog}
            style={styles.buttonStyle}
            labelStyle={styles.labelStyle}
            contentStyle={styles.buttonContentStyle}
          >
            Excluir
          </Button>
        )}
      </ScrollView>
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Excluir lista</Dialog.Title>
          <Dialog.Content>
            <Paragraph>
              tem certeza que quer excluir essa lista e todas as tarefas
              relacionadas?
            </Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>Cancelar</Button>
            <Button onPress={handleDeleteList} textColor="#f66">
              Confirmar
            </Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    padding: 24,
  },
  title: {
    marginTop: 12,
    marginBottom: 24,
    textAlign: "center",
  },
  input: {
    marginBottom: 16,
  },
  supervisionContainer: {
    paddingVertical: 32,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  column: {
    flexDirection: "column",
    justifyContent: "space-between",
  },
  checklistTypeTitle: {
    marginLeft: 2,
    marginVertical: 12,
  },
  checklistTypeText: {
    marginLeft: 4,
  },
  checklistTypeWrapper: {
    marginBottom: 4,
    flexDirection: "row",
    alignItems: "center",
  },
  buttonStyle: {
    marginTop: 12,
    borderRadius: 12,
  },
  buttonContentStyle: {
    height: 48,
  },
  labelStyle: {
    fontSize: 16,
  },
});
