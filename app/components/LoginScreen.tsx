import React, { useCallback, useEffect, useRef, useState } from "react";
import { View, StyleSheet, Image, Keyboard, ScrollView } from "react-native";
import { Realm, useApp } from "@realm/react";
import { Text, Button, TextInput } from "react-native-paper";
import { StatusBar } from "expo-status-bar";

export enum AuthState {
  None,
  Loading,
  LoginError,
  RegisterError,
}

const uri =
  "https://res.cloudinary.com/drj6rtiuz/image/upload/v1670682208/team-checklist_tpun5n.png";

export const LoginScreen = () => {
  const app = useApp();
  const refPassword = useRef(null);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [authState, setAuthState] = useState(AuthState.None);
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      () => {
        setKeyboardVisible(true);
      }
    );
    const keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      () => {
        setKeyboardVisible(false);
      }
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  const handleLogin = useCallback(async () => {
    setAuthState(AuthState.Loading);
    const credentials = Realm.Credentials.emailPassword(email, password);
    try {
      await app.logIn(credentials);
      setAuthState(AuthState.None);
    } catch (e) {
      setAuthState(AuthState.LoginError);
    }
  }, [email, password, setAuthState, app]);

  const handleRegister = useCallback(async () => {
    setAuthState(AuthState.Loading);

    try {
      await app.emailPasswordAuth.registerUser({ email, password });

      const credentials = Realm.Credentials.emailPassword(email, password);

      await app.logIn(credentials);
      setAuthState(AuthState.None);
    } catch (e) {
      setAuthState(AuthState.RegisterError);
    }
  }, [email, password, setAuthState, app]);

  return (
    <View style={styles.container}>
      {!isKeyboardVisible && <Image style={styles.image} source={{ uri }} />}

      <ScrollView
        style={{ flex: 1 }}
        keyboardShouldPersistTaps="always"
        contentContainerStyle={styles.form}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.inputContainer}>
          <TextInput
            value={email}
            label="E-mail"
            mode="outlined"
            autoCorrect={false}
            blurOnSubmit={false}
            returnKeyType="next"
            autoCapitalize="none"
            onChangeText={setEmail}
            textContentType="emailAddress"
            error={
              authState === AuthState.LoginError ||
              authState === AuthState.RegisterError
            }
            onSubmitEditing={() => refPassword.current?.focus()}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            label="Senha"
            mode="outlined"
            value={password}
            ref={refPassword}
            textContentType="password"
            onChangeText={setPassword}
            secureTextEntry={secureTextEntry}
            error={
              authState === AuthState.LoginError ||
              authState === AuthState.RegisterError
            }
            right={
              <TextInput.Icon
                onPress={() => setSecureTextEntry(!secureTextEntry)}
                icon={secureTextEntry ? "eye-off-outline" : "eye-outline"}
              />
            }
          />

          <Text variant="bodySmall" style={{ marginTop: 6 }}>
            * senha de no mínimo 6 digitos
          </Text>
        </View>

        {authState === AuthState.LoginError && (
          <Text style={[styles.error]}>
            Verifique se o e-mail e senha estão corretos
          </Text>
        )}
        {authState === AuthState.RegisterError && (
          <Text style={[styles.error]}>
            Ocorreu um erro ao registrar-se, tente novamente
          </Text>
        )}

        <View style={{ paddingHorizontal: 12, marginTop: 12 }}>
          <Button
            mode="contained"
            onPress={handleLogin}
            style={styles.buttonStyle}
            labelStyle={styles.labelStyle}
            contentStyle={styles.buttonContentStyle}
            disabled={authState === AuthState.Loading}
          >
            Entrar
          </Button>
          <Button
            mode="outlined"
            onPress={handleRegister}
            style={styles.buttonStyle}
            labelStyle={styles.labelStyle}
            contentStyle={styles.buttonContentStyle}
            disabled={authState === AuthState.Loading}
          >
            Criar conta
          </Button>
        </View>
      </ScrollView>
      <StatusBar style="dark" backgroundColor="#fff" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 32,
    backgroundColor: "#fff",
  },
  form: {
    padding: 12,
    paddingBottom: 32,
  },
  inputContainer: {
    paddingVertical: 6,
    paddingHorizontal: 12,
  },
  error: {
    fontSize: 12,
    marginTop: 12,
    color: "#f66",
    textAlign: "center",
  },
  image: {
    flex: 1,
    maxHeight: 300,
  },
  buttonStyle: {
    marginTop: 12,
    borderRadius: 12,
  },
  buttonContentStyle: {
    height: 48,
  },
  labelStyle: {
    fontSize: 16,
  },
});
