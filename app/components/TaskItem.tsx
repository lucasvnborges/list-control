import React from "react";
import Realm from "realm";
import { View, Pressable, StyleSheet } from "react-native";
import { Text, IconButton, useTheme } from "react-native-paper";
import { Task } from "../models/Task";

type TaskItemProps = {
  task: Task & Realm.Object;
  onDelete: () => void;
  onToggleStatus: () => void;
};

export const TaskItem = React.memo<TaskItemProps>(
  ({ task, onDelete, onToggleStatus }) => {
    const theme = useTheme();

    return (
      <View style={styles.task}>
        <Pressable
          onPress={onToggleStatus}
          style={[
            styles.status,
            task.done && { backgroundColor: theme.colors.primaryContainer },
          ]}
        >
          <Text style={styles.icon}>{task.done ? "✓" : "○"}</Text>
        </Pressable>
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>{task.description}</Text>
        </View>
        <IconButton size={20} onPress={onDelete} icon="delete-sweep-outline" />
      </View>
    );
  }
);

const styles = StyleSheet.create({
  task: {
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 8,
    flexDirection: "row",
    borderColor: "#eee",
    backgroundColor: "#fff",
  },
  descriptionContainer: {
    flex: 1,
    paddingVertical: 12,
    justifyContent: "center",
  },
  description: {
    fontSize: 16,
    paddingHorizontal: 10,
  },
  status: {
    width: 48,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    justifyContent: "center",
    backgroundColor: "#c5c5c5",
  },
  icon: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    textAlign: "center",
  },
});
