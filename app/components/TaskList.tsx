import React from "react";
import { FlatList, StyleSheet } from "react-native";
import { Realm } from "@realm/react";
import { Task } from "../models/Task";
import { TaskItem } from "./TaskItem";

type TaskListProps = {
  tasks: Realm.Results<Task & Realm.Object>;
  onDeleteTask: (task: Task & Realm.Object) => void;
  onToggleTaskStatus: (task: Task & Realm.Object) => void;
};

export const TaskList: React.FC<TaskListProps> = ({
  tasks,
  onToggleTaskStatus,
  onDeleteTask,
}) => {
  return (
    <FlatList
      data={tasks}
      showsVerticalScrollIndicator={false}
      contentContainerStyle={styles.listContainer}
      keyExtractor={(task) => task._id.toString()}
      renderItem={({ item }) => (
        <TaskItem
          task={item}
          onDelete={() => onDeleteTask(item)}
          onToggleStatus={() => onToggleTaskStatus(item)}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  listContainer: {
    paddingBottom: 32,
    paddingHorizontal: 12,
  },
});

export default TaskList;
