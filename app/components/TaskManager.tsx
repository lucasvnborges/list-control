import React, { useCallback, useEffect, useMemo } from "react";
import { View, StyleSheet } from "react-native";
import { Task } from "../models/Task";
import { RealmContext } from "../models";
import { AddTaskForm } from "./AddTaskForm";
import { TaskList } from "./TaskList";
import { Appbar } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import { useUser } from "@realm/react";

const { useRealm, useQuery } = RealmContext;

export const TaskManager: React.FC<any> = (props) => {
  const { params } = props.route;

  const realm = useRealm();
  const user = useUser();
  const query = useQuery(Task);
  const navigation = useNavigation();

  const userId = useMemo(() => user.id, [user]);
  const tasks = useMemo(
    () =>
      query
        .sorted("createdAt")
        .filter((task) => task.listId === params.list._id),
    [query]
  );

  useEffect(() => {
    realm.subscriptions.update((mutableSubs) => {
      mutableSubs.add(realm.objects(Task));
    });
  }, [realm, query]);

  const handleAddTask = useCallback(
    (description: string): void => {
      if (!description) {
        return;
      }

      realm.write(() => {
        return new Task(realm, description, userId, params.list._id);
      });
    },
    [realm, userId]
  );

  const handleToggleTaskStatus = useCallback(
    (task: Task & Realm.Object): void => {
      realm.write(() => {
        task.done = !task.done;
      });
    },
    [realm]
  );

  const handleDeleteTask = useCallback(
    (task: Task & Realm.Object): void => {
      realm.write(() => {
        realm.delete(task);
      });
    },
    [realm]
  );

  const handleEditList = () => {
    navigation.navigate("ListManager", { list: params.list });
  };

  return (
    <View style={styles.content}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Lista" titleStyle={styles.appbarTitle} />
        <Appbar.Action icon="playlist-edit" onPress={handleEditList} />
      </Appbar.Header>

      <AddTaskForm onSubmit={handleAddTask} />

      {tasks.length === 0 ? null : (
        <TaskList
          tasks={tasks}
          onDeleteTask={handleDeleteTask}
          onToggleTaskStatus={handleToggleTaskStatus}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  appbarTitle: {
    fontSize: 16,
    marginLeft: 6,
    fontWeight: "700",
  },
});
