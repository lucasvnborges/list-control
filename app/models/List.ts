import { Realm } from "@realm/react";

export class List extends Realm.Object<List> {
  _id: Realm.BSON.ObjectId = new Realm.BSON.ObjectId();
  nomeFazendeiro: string;
  nomeFazenda: string;
  cidade: string;
  nomeSupervisor: string;
  tipoChecklist: string;
  leiteProduzido: string;
  quantidadeGado: string;
  supervisaoMensal: boolean;
  rebanhoAtualizadoEm: Date = new Date();
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  userId!: string;

  static primaryKey = "_id";

  constructor(realm: Realm, data: List, userId?: string) {
    super(realm, { ...data, userId: userId });
  }
}
