import { Realm } from "@realm/react";

export class Task extends Realm.Object<Task> {
  _id: Realm.BSON.ObjectId = new Realm.BSON.ObjectId();
  description!: string;
  done: boolean = false;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  userId!: string;
  listId!: string;

  static primaryKey = "_id";

  constructor(
    realm: Realm,
    description: string,
    userId: string,
    listId: string
  ) {
    super(realm, { description, userId, listId });
  }
}
