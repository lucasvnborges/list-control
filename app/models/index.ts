import { createRealmContext } from "@realm/react";
import { List } from "./List";
import { Task } from "./Task";

const OpenRealmBehaviorConfiguration = {
  type: "openImmediately",
  timeOut: 1000,
  timeOutBehavior: "openLocalRealm",
};

export const RealmContext = createRealmContext({
  schema: [List, Task],
  sync: {
    newRealmFileBehavior: OpenRealmBehaviorConfiguration,
    existingRealmFileBehavior: OpenRealmBehaviorConfiguration,
  },
});
