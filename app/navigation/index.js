import * as React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { AppSync } from "../AppSync";
import { ListManager } from "../components/ListManager";
import { TaskManager } from "../components/TaskManager";

const Stack = createNativeStackNavigator();

function Navigation(props) {
  return (
    <NavigationContainer {...props}>
      <Stack.Navigator
        initialRouteName={
          props.permission === "permitted" ? "Main" : "Permission"
        }
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name="Home" component={AppSync} />
        <Stack.Screen name="ListManager" component={ListManager} />
        <Stack.Screen
          name="TaskManager"
          component={TaskManager}
          options={{
            animation: "slide_from_right",
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigation;
